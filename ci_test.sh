#!/usr/bin/env bash
sudo apt -y purge ruby
sudo apt-add-repository -y ppa:brightbox/ruby-ng
sudo apt update
sudo apt install -y ruby2.4 ruby2.4-dev build-essential patch sqlite3 libsqlite3-dev libssl-dev libreadline-dev libgdbm-dev zlib1g-dev libxml2 liblzma-dev git 2>&1 >> /tmp/script.log

sudo rm -rf /opt/project 2>&1 >> /tmp/script.log
cd /opt
rm -rf project.tar.gz
wget https://gitlab.com/gowtham-sai/test_project/-/jobs/56575702/artifacts/raw/project.tar.gz 2>&1 >> /tmp/script.log
mkdir -p /opt/project
sudo tar -xvzf project.tar.gz -C project
cd /opt/project
gem install bundler 2>&1 >> /tmp/script.log
# sudo bundle install  2>&1 >> /tmp/script.log
bundle exec rspec 2>&1 >> /tmp/script.log

